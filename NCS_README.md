### First React/Electron/Azure App
# by Neil Stackman

# Template - https://www.section.io/engineering-education/desktop-application-with-react/

* Use scaffold to build react app... npx create-react-app ns_electron_react_1
* Install electron and devtools... npm i -D electron electron-is-dev
* Create vanilla electron.js file
* Add a 'main' script
* Add Packages to launch app as Electron... npm i -D concurrently wait-on
* Begin creating an example app... npm i react-open-weather

## And we are here... time to start working with AD
# https://docs.microsoft.com/en-us/azure/active-directory/develop/tutorial-v2-react
<!--
App ID: sx72d3fssWNvZU.Ge_d8t~~H9A.47CI~jQ
Object ID: 3f2f9513-df77-4f5c-9239-4d7a3787cfa2
Tenant: 63b0ad9b-9b77-4098-aef4-05e141fb1ddd
-->
* npm install @azure/msal-browser @azure/msal-react
* npm install react-bootstrap bootstrap
* create authConfig.js to template

## Success
* we can connect to Azure Authentication
* we can request a Token
* we can use the token to make a Graph API call

<!--
Notes: Markdown Syntax...
https://confluence.atlassian.com/bitbucketserver/markdown-syntax-guide-776639995.html#:~:text=Markdown%20syntax%201%20Headings%202%20Paragraphs.%20Paragraphs%20are,Links%20to%20external%20websites.%20...%20More%20items...%20
-->

## Step 2. What can we do with Graph?

