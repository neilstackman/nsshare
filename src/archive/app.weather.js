/* Weather App... */
import ReactWeather, { useOpenWeather } from 'react-open-weather';

function App() {

  const { data, isLoading, errorMessage } = useOpenWeather({
    key: '59ecf9da57f5ce5ea2ddfd0b674a8254',
    lat: 51.768680,
    lon: -0.238110,
    lang: 'en',
    unit: 'metric', // values are (metric, standard, imperial)
  });

  return ( 
    <div className = "App"> 
      <ReactWeather isLoading = { isLoading }
      errorMessage = { errorMessage }
      data = { data }
      lang = "en"
      locationLabel = "Weather at Airbase"
      unitsLabels = { { temperature: 'C', windSpeed: 'Km/h' } }
      showForecast 
      />
    </div>
  );

}

export default App;