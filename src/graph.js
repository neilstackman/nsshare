import { graphConfig } from "./authConfig";

/*
 * Attaches a given access token to a Microsoft Graph API call. Returns information about the user
 */
export async function callMsGraph(accessToken, resource, id) {
  console.log('callMsGraph',resource,id);

  const headers = new Headers();
  const bearer = `Bearer ${accessToken}`;

  headers.append("Authorization", bearer);

  const options = {
    method: "GET",
    headers: headers,
  };

  let sEndpoint = graphConfig[resource];
  // replace :id 
  if(id){
    sEndpoint = sEndpoint.replace(':id',id);
  }
  console.log('fetching',sEndpoint);

  if (resource === "graphPhoto") {
    return fetch(sEndpoint, options)
      .then((response) => {
        const reader = response.body.getReader();
        return new ReadableStream({
          start(controller) {
            return pump();
            function pump() {
              return reader.read().then(({ done, value }) => {
                // When no more data needs to be consumed, close the stream
                if (done) {
                  controller.close();
                  return;
                }
                // Enqueue the next data chunk into our target stream
                controller.enqueue(value);
                return pump();
              });
            }
          },
        });
      })
      .then((stream) => new Response(stream))
      .then((response) => response.blob())
      .then((blob) => URL.createObjectURL(blob))
      .catch((err) => console.error(err));
  } else {
    return fetch(sEndpoint, options)
      .then((response) => {
        return response.json();
      })
      .catch((err) => {
        console.error(err);
      });
  }
  
}
