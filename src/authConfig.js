export const msalConfig = {
    auth: {
        clientId: "dee7bf5f-c82b-4caa-a005-dcf4cac89476",
        authority: "https://login.microsoftonline.com/63b0ad9b-9b77-4098-aef4-05e141fb1ddd",
        redirectUri: "http://localhost:3000"
    },
    cache: {
        cacheLocation: "sessionStorage",
        storeAuthStateInCookie: false
    }
}

// Add scopes here for ID token to be used at Microsoft identity platform endpoints.
export const loginRequest = {
 scopes: ["User.Read"/*, "User.Read.All", "Group.Read", "Group.Read.All"*/]
};

// Add the endpoints here for Microsoft Graph API services you'd like to use.
export const graphConfig = {
    graphMe: "https://graph.microsoft.com/v1.0/me",
    graphPhoto: "https://graph.microsoft.com/v1.0/me/photo/$value",
    graphPhotos: "https://graph.microsoft.com/v1.0/me/photos",
    // graphGroups: "https://graph.microsoft.com/v1.0/users/:id/memberOf"
    graphGroups: "https://graph.microsoft.com/v1.0/me/memberOf"
};