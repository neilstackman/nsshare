import React from "react";
import { AbdNavbar } from "./components/AbdNavbar";
import { AuthenticatedTemplate, UnauthenticatedTemplate } from "@azure/msal-react";
import { PageLayout } from "./components/PageLayout";
import './css/app.css';

export const App = (props) => {
  return (
    <>
      <AuthenticatedTemplate>
      <AbdNavbar></AbdNavbar>
      <PageLayout></PageLayout>
      </AuthenticatedTemplate>

      <UnauthenticatedTemplate>
      <AbdNavbar></AbdNavbar>
      <p>You are not signed in! Please sign in.</p>
      </UnauthenticatedTemplate>
    </>
  );

}
