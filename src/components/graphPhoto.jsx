import React from "react";

export const GraphPhoto = (props) => {
    return (
        <div id="graphphoto-div">
            <img src={props.graphPhoto} alt="my photo"></img>
        </div>
    );
};