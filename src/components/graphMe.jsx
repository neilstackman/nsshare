import React from "react";

export const GraphMe = (props) => {
    return (
        <div id="graphme-div">
            <p><strong>First Name: </strong> {props.graphMe.givenName}</p>
            <p><strong>Last Name: </strong> {props.graphMe.surname}</p>
            <p><strong>Email: </strong> {props.graphMe.userPrincipalName}</p>
            <p><strong>Id: </strong> {props.graphMe.id}</p>
        </div>
    );
};