import React from "react";
import { useMsal } from "@azure/msal-react";
import { loginRequest } from "../authConfig";
import Button from "react-bootstrap/Button";

function handleLogin(instance) {
  instance.loginRedirect(loginRequest).catch((e) => {
    console.error(e);
  });
}

/**
 * Renders a button which, when selected, will redirect the page to the login prompt
 */
export const SignInButton = (props) => {
  const { instance } = useMsal();

  return (
    <Button
      variant="success"
      className="ml-1"
      onClick={() => handleLogin(instance)}
    >
      Sign in
    </Button>
  );
};
