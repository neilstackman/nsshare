import React, { useEffect, useState } from "react";
import { useMsal, useIsAuthenticated } from "@azure/msal-react";
import { GraphMe } from "../components/graphMe";
import { GraphPhoto } from "../components/graphPhoto";
import { GraphGroups } from "../components/graphGroups";
import { callMsGraph } from "../graph";

export const AbdMyProfile = (props) => {
  
  const { instance, accounts, inProgress } = useMsal();
  const [accessToken, setAccessToken] = useState(null);
  const [myId, setMyId] = useState(null);
  const [myName, setMyName] = useState(null);  
  const [graphMe, setGraphMe] = useState(null);
  const [graphGroups, setGraphGroups] = useState(null);
  const [graphPhoto, setGraphPhoto] = useState(null);

  const getGraphMe = (() => {
    console.log('getGraphMe starts');

    return new Promise((resolve, reject) => {

      callMsGraph(accessToken, "graphMe")
      .then((meResponse) => {
        console.log('callMsGraph.graphMe success',meResponse);
        setGraphMe(meResponse);
        setMyId(meResponse.id);
        resolve();
      }).catch((e) => { 
        console.log('callMsGraph.graphMe failed',e); 
        setGraphMe(null);
        setMyId(null);
        reject();
      });
  
    });
  });

  const getGraphPhoto = (() => {
    console.log('getGraphPhoto starts');
    return new Promise((resolve, reject) => {

      callMsGraph(accessToken, "graphPhoto")
      .then((photoResponse) => {
        console.log('callMsGraph.graphPhoto success',photoResponse);
        setGraphPhoto(photoResponse);
        resolve();
      }).catch((e) => { 
        console.log('callMsGraph.graphPhoto failed',e); 
        setGraphPhoto(null);
        reject();
      });
  
    });
  });

  const getGraphGroups = (() => {
    console.log('getGraphGroups starts', myId);
    return new Promise((resolve, reject) => {

      callMsGraph(accessToken, "graphGroups", myId )
      .then((groupsResponse) => {
        console.log('callMsGraph.graphGroups success',groupsResponse);
        setGraphGroups(groupsResponse);
        resolve();
      }).catch((e) => { 
        console.log('callMsGraph.graphGroups failed',e); 
        setGraphGroups(null);
        reject();
      });

    });      
  });

  useEffect(() => {
    setAccessToken(null);
    if (inProgress === "none" && accounts.length > 0) {
        setMyName(accounts[0] && accounts[0].name);
        console.log('useEffect',accounts[0],myName);
        // Retrieve an access token
        const temp = instance.acquireTokenSilent({
            account: accounts[0],
            scopes: ["User.Read"]
        }).then(response => {
            if (response.accessToken) {
              setAccessToken(response.accessToken);
              getGraphMe()
              .then( (value) => getGraphPhoto(value) )
              .then( (value) => getGraphGroups(value) )
              .catch((e) => {
                console.log('Error Retrieving MyProfile',e);
              });
              return response.accessToken;
            }
            return null; 
        });
    }
  }, [inProgress, accounts, instance]);

  return (
    <>
      <h5 className="card-title">Welcome {myName}</h5>
      {
        accessToken && graphMe &&
        <React.Fragment>
          <GraphMe graphMe={graphMe} />
          <GraphPhoto graphPhoto={graphPhoto} />
          <GraphGroups graphGroups={graphGroups} />
        </React.Fragment>
      }
    </>
  );
};
