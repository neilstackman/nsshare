import React from "react";
import {
  BrowserRouter as Router,
  Switch,
  Route,
  useParams,
} from "react-router-dom";
import {
  Nav,
  Navbar,
  NavDropdown,
  Form,
  FormControl,
  Button,
} from "react-bootstrap";
import { useIsAuthenticated } from "@azure/msal-react";
import { SignOutButton } from "./SignOutButton";
import { SignInButton } from "./SignInButton";

import { PageHome } from "../pages/PageHome";
import { PageProfile } from "../pages/PageProfile";
import { Page1 } from "../pages/Page1";
import { Page2 } from "../pages/Page2";
import { Page3 } from "../pages/Page3";

import logo from "../images/logo.png";

export const AbdNavbar = (props) => {
  const isAuthenticated = useIsAuthenticated();

  return (
    <>
      <Router>
        <Navbar bg="light" expand="lg">
          <Navbar.Brand href="/">
            <img src={logo} alt="Air Business" />
            {/* <p>Air Business Distribution - NS Hub</p> */}
          </Navbar.Brand>
          <Navbar.Toggle aria-controls="basic-navbar-nav" />
          <Navbar.Collapse id="basic-navbar-nav">
              { isAuthenticated ? <Nav className="mr-auto">
              <Nav.Link className="mt-2 mb-0 pb-0" href="/">Home</Nav.Link>
              <Nav.Link className="mt-2 mb-0 pb-0" href="/profile">My Profile</Nav.Link>
              <NavDropdown className="mt-2 mb-0 pb-0" title="Pages" id="basic-nav-dropdown">
                <NavDropdown.Item href="/page1">Page 1</NavDropdown.Item>
                <NavDropdown.Item href="/page2">Page 2</NavDropdown.Item>
                <NavDropdown.Item href="/page3">Page 3</NavDropdown.Item>
                <NavDropdown.Divider />
                <NavDropdown.Item href="/page1">Page 1 again</NavDropdown.Item>
              </NavDropdown></Nav> : <Nav className="mr-auto">
              <Nav.Link className="mt-2 mb-0 pb-0" href="/">Home</Nav.Link>
              </Nav>}
            
            <Form inline>
              <FormControl
                type="text"
                placeholder="Search"
                className="mr-sm-2"
              />
              <Button variant="outline-success">Search</Button>
              {isAuthenticated ? <span>Signed In</span> : null }
              {isAuthenticated ? <SignOutButton /> : <SignInButton />}
            </Form>
          </Navbar.Collapse>
        </Navbar>
        <br />
        <Switch>
          <Route path="/profile" render={(props) => <PageProfile />} />
          <Route path="/page1" render={(props) => <Page1 />} />
          <Route path="/page2" render={(props) => <Page2 />} />
          <Route path="/page3" render={(props) => <Page3 />} />
          <Route path="/" render={(props) => <PageHome />} />
        </Switch>
      </Router>
      {props.children}
    </>
  );
};
