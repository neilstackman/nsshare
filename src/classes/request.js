export class cREQUEST {

    constructor(params) {
    this.id = "";
    this.name = "";

    if (Object.prototype.toString.call(params) === "[object Object]") {
      this.data = params;
      for (let [key, value] of Object.entries(params)) {
        this[key] = value;
      }
    } else {
      this.id = params;
      this.name = "";
    }

    this.endPoint = "http://myRequestEndpoint";
  }

  show(attr) {
    return this.data[attr] ? this.data[attr] : null;
  }

}
