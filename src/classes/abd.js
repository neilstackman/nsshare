import { cJOBs } from './job.js';
import { cREQUEST } from './request.js';

export class cABD {

  constructor(token) {
    this.token = token;
    this.jobs = new cJOBs({ id: "1", name: "job 1" });
    this.request = new cREQUEST({ id: "1", name: "request 1" });
  }

  show() {
    return {
      token: this.token,
      job: this.job,
    };
  }

}