export class cJOBs {

  constructor(params) {
    this.id = "";
    this.name = "";
    this.jobs = [];

    if (Object.prototype.toString.call(params) === "[object Object]") {
      this.data = params;
      for (let [key, value] of Object.entries(params)) {
        this[key] = value;
      }
    } else {
      this.id = params;
      this.name = "";
    }

    this.createEndPoint = "http://job.create.endpoint";
    this.readEndPoint = "http://job.read.endpoint";
    this.updateEndPoint = "http://job.update.endpoint";
    this.deleteEndPoint = "http://job.delete.endpoint";
    this.listEndPoint = "http://job.list.endpoint";
  }

  create(job) {
    this.jobs.push(new cJOB);
    return 'the created job';
  }
  read(jobID) {
    /*
    this.jobs.filter((jobID) => {

    });
    */
    return 'the job';
  }
  update(jobID) {
    /* this.jobs.patch('the updated job'); */
    return 'the updated job';
  }
  delete(jobID) {
    /* this.jobs.pop('the deleted job'); */
    return 'the deleted job';
  }
  list(filter) {
    return 'a list of jobs';
  }
  sync() {
    /* make sure that internal array matches database*/
  }

  show(attr) {
    return this.data[attr] ? this.data[attr] : null;
  }

}

class cJOB {

  constructor(params) {
    this.id = "";
    this.name = "";

    if (Object.prototype.toString.call(params) === "[object Object]") {
      this.data = params;
      for (let [key, value] of Object.entries(params)) {
        this[key] = value;
      }
    } else {
      this.id = params;
      this.name = "";
    }

    this.endPoint = "http://myJobEndpoint";
  }

  show(attr) {
    return this.data[attr] ? this.data[attr] : null;
  }

}
